import { createAction, props } from '@ngrx/store';
import { Code } from '../../pages/generator/generator.models';

export const setGeneratorState = createAction('[Generator Status] set Code', props<{code: Code, matrix: string[][]}>());
export const clean = createAction('[Generator Status] Clean');
