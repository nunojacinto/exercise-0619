import { createReducer, on } from '@ngrx/store';
import { Code } from '../../pages/generator/generator.models';
import { setGeneratorState, clean } from './generator.actions';

export const initialState: {code: Code, matrix: string[][]} = null;
 
const _GeneratorReducer = createReducer(
  initialState,
  on(setGeneratorState, (state, {code, matrix}) => { return {code: code, matrix: matrix}}),
   on(clean, (state) => initialState)
);
 
export function generatorReducer(state, action) {
  return _GeneratorReducer(state, action);
}
