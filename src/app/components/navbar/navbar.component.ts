import { Component, OnInit } from '@angular/core';
export interface Link {
  name: string,
  url: string
}


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  links: Link[] = [
    {
      name: 'Generator',
      url: "/generator",
    },
    {
      name: 'Payments',
      url: "/payments",
    }
  ];

  ngOnInit(): void {
  }

}
