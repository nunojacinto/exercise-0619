export interface myPaymentItem {
    name: string,
    amount: number,
    code: string,
    grid: string[][]
  }
  