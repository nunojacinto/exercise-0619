import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { setGeneratorState } from 'src/app/state/generator/generator.actions';
import { Code, MatrixGenerator } from './generator.models';



@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.scss']
})
export class GeneratorComponent implements OnInit {

  matrix: string[][];
  matrixGenerator: MatrixGenerator<String>;
  generatorForm: FormGroup;

  running = false;

  code: Code = { number1: 0, number2: 0 }

  constructor( private store: Store<{ generator: {code: Code, matrix: string[][]}}> ) {
    this.matrixGenerator = new MatrixGenerator<String>(10, 10);
    this.matrix = this.matrixGenerator.initial("a");
    this.generatorForm = new FormGroup({
      char: new FormControl({ value: null, disabled: false },
        [Validators.maxLength(1), Validators.pattern('[a-z]{1}')])
    });


    store.pipe(select('generator')).subscribe(generator => {
      if(generator) {
        this.running = true;
        this.code = generator.code;
        this.matrix = generator.matrix;
      }
    });

  }

  ngOnInit(): void {
  }

  startGenerator(): void {
    this._runGenerator();
    setInterval(() => {
      this._runGenerator();
    }, 2000)
  }


  private _runGenerator() {
    this.running = true;
    if (this.generatorForm.value.char && (this.generatorForm.valid)) {
      this.generatorForm.controls['char'].disable();
      setTimeout(() => {
        this.generatorForm.controls['char'].setValue(null);
        this.generatorForm.controls['char'].enable();
      }, 4000)
      this.matrix = this.matrixGenerator.generateRandom(this.generatorForm.value.char, 20);
    } else {
      this.matrix = this.matrixGenerator.generateRandom();
    }

    this.calcCode();
  }


  calcCode() {
    const seconds = new Date().getSeconds();
    const formatSeconds = ("0" + seconds).slice(-2);
    const v = formatSeconds[0];
    const c = formatSeconds[1];
    const cell1 = this.matrix[c][v];
    const cell2 = this.matrix[v][c];

    var count_1 = 0;
    var count_2 = 0;
    this.matrix.forEach(row => {
      row.forEach(cell => {
        if (cell == cell1) count_1++;
        if (cell == cell2) count_2++;
      });
    })

    this.code = {
      number1: this._divideByLowestInterger(count_1, 9),
      number2: this._divideByLowestInterger(count_2, 9)
    }

    this.store.dispatch(setGeneratorState({code: this.code, matrix: this.matrix}));
  
  }

  private _divideByLowestInterger(n: number, lowerthan: number): number {
    // *CAUTION! this will only works for 10x10 matrix
    if (n <= lowerthan) return n;
    for (let i = 2; i <= 10; i++) { // *HERE (And note that -> 9,7 is not a valid result)
      if (n / i <= lowerthan) {
        return Math.ceil(n / i);
      }
    }
  }


}
