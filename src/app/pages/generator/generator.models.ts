export interface Code {
    number1: number;
    number2: number;
}



export class MatrixGenerator<T> {
    alphabet: string = "abcdefghijklmnopqrstuvwxyz";
    rows: number;
    columns: number;

    constructor(rows: number, columns: number) {
        this.rows = rows;
        this.columns = columns;
    }

    initial(initialValue: T) {
        const initialMatrix = [];
        for (var i: number = 0; i < this.rows; i++) {
            initialMatrix.push([])
            for (var j: number = 0; j < this.columns; j++) {
                initialMatrix[i][j] = initialValue;
            }
        }
        return initialMatrix;
    }

    generateRandom(value?: string, weigth?: number) {
        const random = [];
        for (var i: number = 0; i < this.rows; i++) {
            random[i] = [];
            for (var j: number = 0; j < this.columns; j++) {
                random[i][j] = this.getRadomValue(value, weigth);
            }
        }
        return random;
    }

    private getRadomValue(value, weigth) {
        const alphabetLength = this.alphabet.length;
        if (value && (Math.random() * 100 > (100 - weigth))) { // this will not generate exactly weight% !!
            return value;
        }
        return this.alphabet.charAt(Math.floor(Math.random() * alphabetLength))

    }

}