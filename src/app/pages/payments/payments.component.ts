import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Code } from '../generator/generator.models';
import { myPaymentItem } from 'src/app/models/payments.models';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PaymentsService } from 'src/app/services/payments.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  code: Code;
  matrix: string[][];
  paymentForm: FormGroup;

  paymentList: myPaymentItem[];

  constructor(
    private paymentService: PaymentsService,
    private store: Store<{ generator: {code: Code, matrix: string[][]}}>) {
    store.pipe(select('generator')).subscribe(generator => {
      if(generator) {
        this.code = generator.code;
        this.matrix = generator.matrix;
      }
    });

    this.paymentService.list.subscribe(
      list => {
        this.paymentList = list;
      }
    )

    this.paymentForm = new FormGroup({
      name: new FormControl({ value: null, disabled: false }, [Validators.required]),
      amount: new FormControl({ value: null, disabled: false }, [Validators.required]),
    });
  }

  ngOnInit(): void {
  }

  addPayment() {
    const CodeString = this.code.number1+''+this.code.number2;
    this.paymentService.addPayment(this.paymentForm.value.name,  this.paymentForm.value.amount, CodeString, this.matrix).subscribe(
      resp => {
        console.log("got response from server", resp);
      },
      error => console.log(error)
    )
  }

}
