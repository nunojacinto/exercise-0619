import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { myPaymentItem } from '../models/payments.models';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  list: BehaviorSubject<myPaymentItem[]> = new BehaviorSubject([]);;

  constructor() { }

  addPayment(name: string, amount: number, code: string, grid: string[][]): Observable<Boolean> {
    /** method that will call to add payment, for test purposes it will always return true (success)*/
    /** TODO: Add mapper here depending on API request and response */
    console.info("[Payment Service] Requesting to add Payment", name, amount, code, grid);
    return of(true).pipe(resp => {
      if (resp) {
        // if true store in service... we could also create a store using ngrx
        this.list.next(this.list.getValue().concat([{
          name: name,
          amount: amount,
          code: code,
          grid: grid
        }]));
      }
      return resp;
    }
    )
  }
}
