import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'generator', loadChildren: () => import('./pages/generator/generator.module').then(m =>m.GeneratorModule)},
  { path: 'payments',loadChildren: () => import('./pages/payments/payments.module').then(m =>m.PaymentsModule)},
  { path: '**',   redirectTo: '/generator', pathMatch: 'full' }
  // { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
